#= require angular
#= require angular-resource
#= require angular-rails-templates
#= require ng-file-upload-all
#= require ui-bootstrap-tpls-1.3.1
#= require angular-confirm

#= require ./module
#= require_tree ./templates
#= require_tree ./pictures
