require_relative 'base_controller'

module AppComponent
  module Admins
    class DashboardController < BaseController
      def index; end
    end
  end
end